# Secondary Market - Operator management

## Story: Add operator

**As a** user,\
**I want** to add an operator for my domain

**Scenario 1**: see Permissions button\
**Given** that a user has a connected wallet,\
**And** they own a domain,\
**When** the user is on the domain detail page,\
**Then** they should see a Permissions button.

**Scenario 2**: see existing operators\
**Given** that a user has a connected wallet,\
**And** they own a domain,\
**When** they are on the domain details page,\
**And** click the Permissions button,\
**Then** they open a dialog where they can see the addresses of the current operators.

**Scenario 3**: add an operator of a domain\
**Given** that a user has a connected wallet,\
**And** they own a domain,\
**When** they are on the domain details page,\
**And** click the Permission button,\
**Then** they can enter a valid Tezos address in the "New Operator" textbox,\
**And** and click on the "add" icon,\
**And** save the new operator by clicking on "Save" and signing the operation.

**Scenario 4**: remove an operator of a domain\
**Given** that a user has a connected wallet,\
**And** they own a domain,\
**And** the domain has at least one operator,\
**When** they are on the domain details page,\
**And** click the Permission button,\
**Then** they click on the "remove" button next to an operator,\
**And** remove the new operator by clicking on "Save" and signing the operation.

## Story: Change domain as operator

**As an** operator of a domain,\
**I want** to see the domains I am operator of

**Given** that a user has a connected wallet,\
**And** is an operator of a domain,\
**When** they go to the domain details page,\
**Then** they should be able to Edit the domain,\
**And** transfer the domain,\
**And** change operators of the domain,\
**And** setup a website,\
**And** renew the domain,\
**And** add subdomains.

## Story: See domains where user is operator

**As an** operator of a domain,\
**I want** to make changes to that domain

**Scenario 1**: see domains that where I'm operator of,\
**Given** that a user has a connected wallet,\
**And** is an operator of a domain,\
**Then** on the domain list page I should see the domains that I am operator of,\
**And** they should be marked with an "op" badge.
