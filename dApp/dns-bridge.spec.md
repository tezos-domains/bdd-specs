# DNS Bridge - claim a DNS owned domain 

## Story: Search for a DNS domain

**As a** user,\
**I want** to be able to search for my DNS domain,\
**So that** I can claim it as my own.

**Given** that a user is on the home page,\
**And** that the user owns the DNS domain: `crazy-fox.xyz`,\
**When** they search for `crazy-fox.xyz`,\
**Then** they should see the domain that was searched for,\
**And** they can click the "Claim" button.

## Story: Claim a DNS domain

**As a** user,\
**I want** to be able to claim my DNS domain,\
**So that** I can use it throughout the tezos ecosystem to transfer funds.

**Background**:\
**Given** that the user has a connected wallet.

**Given** that a user is on the search results page,\
**When** they click the "Claim" button,\
**Then** they should see the DNS registration page.

**Scenario 1:** the domain does not have DNSSEC enabled.\
**Given** that a user is on the DNS registration page,\
**When** the domain doesn't have DNSSEC,\
**And** the user clicks "Check again",\
**Then** they should see a warning message about failed DNSSEC verification.

**Scenario 2:** the domain does not have a correct TXT record.\
**Given** that a user is on the DNS registration page,\
**When** the domain doesn't have a correct TXT record value,\
**And** the user clicks "Check again",\
**Then** they should see a warning message about invalid TXT record value.

**Scenario 3:** the domain has correct DNS configuration setup.\
**Given** that a user is on the DNS registration page,\
**When** the domain has DNSSEC and valid TXT record,\
**Then** they should see the "Register" step,\
**And** the price of claiming the DNS domain.

**Scenario 4:** the domain has correct DNS configuration setup and user tries to register it.\
**Given** that a user is on the DNS registration page,\
**When** the domain has DNSSEC and valid TXT record,\
**And** they click the "Register" button,\
**Then** the user should be allowed to register the domain.


## Story: View details of a claimed DNS domain

**As a** user,\
**I want** to be able to view my claimed DNS domain,\
**So that** I can make changes to it.

**Background**:\
**Given** that the user has a connected wallet.

**Given** that a user is on their dashboard,\
**And** see their claimed DNS domain,\
**When** they click the "Manage" button,\
**Then** they should see domain detail page,\
**And** the actions available should be: "Edit", "Transfer Ownership", "Reverse Record", "Website", "Subdomains"
