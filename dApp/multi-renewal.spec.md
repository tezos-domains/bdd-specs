# Domain multi renewal

## Story: See warning about domain expiring

**As a** user,\
**I want** to be able to see a warning when my domains are expiring,\
**So that** I can renew them in time

**Scenario 1**: a user can see an `info` message that domains are expiring.\
**Given** that the user has a connected wallet,\
**And** the user has some domains expiring in less than 90 days,\
**When** the user opens the home page,\
**Then** the user should see an `info` message box with a "Renew" button.

**Scenario 2**: a user can see a `warning` message that domains are expiring.\
**Given** that the user has a connected wallet,\
**And** the user has some domains expiring in less than 30 days,\
**When** the user opens the home page,\
**Then** the user should see a `warning` message with a "Renew" button.

## Story: Navigate to the multi-renew page

**As a** user,\
**I want** to quickly get to the list of expiring domains,\
**So that** I can renew multiple at the same time

**Scenario 1**: a user can navigate to the "Up for renewal" list from the `info / warning` message on their home page\
**Given** that the user has a connected wallet,\
**And** the user has some expiring domains,\
**When** the user is on the home page,\
**And** the user clicks on the "Renew" button on the `info / warning` message on the home page,\
**Then** the user is redirected to a page where all expiring domains are selected,\
**And** there's a button titled "Renew Domains" to trigger the Multi-Renewal process.

**Scenario 2**: a user is on the domain list page\
**Given** that the user has a connected wallet,\
**When** the user is on the home page,\
**And** the user sees the info or warning box,\
**And** the user clicks on the "Renew" button,\
**Then** the user is redirected to a page where all domains are selected,\
**And** there's a button to trigger the Multi-Renewal process

## Story: Navigate to the multi-renew page from email

**Background**:\
**Given** that the user has received an email about expiring domains

**Scenario 1**: a user is already connected\
**Given** that a user has a wallet connected,\
**When** they click on the "Renew" link from their email,\
**Then** the user should land on the multi-renew page,\
**And** all domains expiring in 90 days are selected.


**Scenario 2**: a user is not already connected\
**Given** that a user does not have a wallet connected,\
**When** they click on the "Renew" link from their email,\
**Then** the user should land on the multi-renew page,\
**And** the Connect dialog should open.


## Story: Renew multiple domains
**As a** user,\
**I want** renew multiple domains

**Background**: \
**Given** that a user with a wallet connected

**Scenario 1**: \
**Given** that a user has navigated to the domain list page,\
**When** the user selects at least one domain,\
**Then** show a "Renew Domains" button.

**Scenario 2**: \
**Given** that a user has navigated to the domain list page,\
**And** the user selects at least one domain,\
**When** the user clicks "Renew Domains",\
**Then** show a dialog where the user can change the renewal period,\
**And** can renew all selected domains at once.
