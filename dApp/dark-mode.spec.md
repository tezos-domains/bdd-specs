# Dark Mode 

## Story: Automatically use Dark/Light theme based on OS preferences 

**As a** user,\
**I want** to see the website according to my OS preferences,\
**So that** I am happy.

**Given** that a user has their OS or Browser preference set to Dark theme,\
**When** they load any page of the dApp,\
**Then** they should the dark theme version of the website.

## Story: Change Dark/Light theme in the application

**As a** user,\
**I want** to be able to change between the Dark/Light theme
**So that** I can view the website the way I prefer.

**Given** that a user is on any page,\
**When** they click the "Moon/Sun" button,\
**Then** they should see the corresponding color scheme.

**Given** that a user has changed the desired theme,\
**When** they load the application at a later time,\
**Then** they should see the previously selected color scheme.
