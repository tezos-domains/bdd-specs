# Airdrop claim process 

## Story: See message about airdrop being in progress

**As a** user,\
**I want** to see a message about the airdrop being in progress,\
**So that** I can claim my tokens 

**Scenario 1** as a user I, can see a message that the airdrop is in progress.\
**Given** that a user enters tezos.domains or app.tezos.domains,\
**When** the user opens the home page,\
**Then** the user should see a message box with a "Go to Airdrop" button.


## Story: See airdrop claim page

**As a** user,\
**I want** to see the airdrop claim page,\
**So that** I can claim my tokens.

**Scenario 1**: User is prompted to connect\
**Given** that a user doesn't have a connected wallet,\
**When** the user enters the claim page,\
**Then** the user is prompted to connect their wallet.

**Scenario 2**: User that hasn't claimed their tokens is shown the claim page\
**Given** that a user has a wallet connected,\
**And** the user has not yet claimed their tokens,\
**And** the user has tokens to be claimed,\
**When** the user enters the claim page,\
**Then** the user is shown the total number of tokens they can claim.

**Scenario 2**: User that has claimed their tokens, is shown the claim page\
**Given** that a user has a wallet connected,\
**And** the user has already claimed their tokens,\
**When** the user enters the claim page,\
**Then** the user is shown that they can no longer claim any tokens.


## Story: Claiming process

**As a** user,\
**I want** to understand the claiming process step by step,\
**So that** I understand what and why is happening.

**Background**:\
**Given** that a user has a wallet connected,\
**And** has tokens to claim

**When** the user opens the airdrop claim process page,\
**Then** the user should see a step-by-step wizard that explains the process.

**When** the user opens the airdrop claim process page,\
**Then** the user should see a step-by-step wizard that explains the process,\
**And** the wizard contains an introductory step with the number of tokens they have,\
**And** the wizard contains a step showing the toke distribution,\
**And** the wizard contains a step showing the DAO Constitution,\
**And** the wizard contains a step where the user can delegate their tokens.

**Given** the user is on the Constitution step,\
**When** the user clicks "Reject" on any of the Articles,\
**Then** the user sees a message letting them know that the process cannot continue,\
**And** the user is returned to the start of the airdrop claim process.

**Given** that a user has advanced through the airdrop claim page wizard,\
**When** the user arrives at the Delegation step,\
**Then** the user is prompted to select a delegate for their tokens.


## Story: Selecting a delegate

**As a** user,\
**I want** to delegate my tokens,\
**So that** I have an easier process of voting

**Background**:\
**Given** that a user has a wallet connected,\
**And** has tokens to claim

**Scenario 1**:\
**Given** that a user is on the Delegation step,\
**When** the user wants to select a delegate,\
**Then** the user sees the option to delegate to a group of preselected people.

**Scenario 2**:\
**Given** that a user is on the Delegation step,\
**When** the user wants to become a delegate,\
**Then** the user sees an option to enter a tezos address or a Tezos Domains name,\
**And** the user can delegate their tokens to that address.

## Story: Success
**As a** user,\
**I want** to see a message,\
**So that** I know my tokens have been successfully claimed.

**Given** that a user has a wallet connected,\
**And** the user has tokens to claim,\
**And** the user went through the airdrop claim wizard,\
**And** the user has successfully claimed their tokens,\
**Then** the user should see a "Congratulations" page,\
**And** can share this on social networks.